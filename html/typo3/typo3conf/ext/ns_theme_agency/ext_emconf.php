<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "ns_theme_agency".
 *
 * Auto generated 08-06-2022 16:12
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => '[NITSAN] T3 Agency TYPO3 Template',
  'description' => 'Agency TYPO3 template is an ultimate tool to kickstart your project, either a software development company, or a product startup or any new business. Live-Demo: https://demo.t3terminal.com/?theme=t3t-agency PRO version: https://t3terminal.com/t3-agency-free-business-typo3-template',
  'category' => 'templates',
  'version' => '4.0.2',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => false,
  'author' => 'Team NITSAN',
  'author_email' => 'info@nitsan.in',
  'author_company' => 'NITSAN Technologies Pvt Ltd',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.0.0-11.5.99',
      'ns_basetheme' => '10.0.0-11.5.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

